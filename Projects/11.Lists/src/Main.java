public class Main {
    public static void main(String[] args) {

        ArrayList list = new ArrayList();
        list.add(7);
        list.add(10);
        list.add(33);


        //tests
        System.out.println(list.size());
        list.add(4, 55);
        System.out.println("-----");
        System.out.println(list.size());
        System.out.println("-----");
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));
        System.out.println(list.get(4));

    }
}