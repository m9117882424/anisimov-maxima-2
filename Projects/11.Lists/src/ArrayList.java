//список на основе массива

public class ArrayList {

    private static final int DEFAULT_SIZE = 10;

    //хранилище элементов
    private int[] elements;

    //количество фактических элементов в списке
    private int size;

    public ArrayList() {
        //при создании списка, создали внутри массив на 10 элементов
        this.elements = new int[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     *
     * @param element добавляемый элемент
     */
    public void add(int element) {
        if (isFullArray()) {
            //создаем новый массив
            resize();
        }
        this.elements[size++] = element;
    }

    private void resize() {
        int[] newArray = new int[this.elements.length + elements.length / 2];
        //копируем из старого в новый
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        //заменяем ссылку на старый массив ссылкой на новый
        //старый удалит JM
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }


    /**
     * Получение элемента по индексу
     *
     * @param index индекс элемента
     * @return элемент, который был добавлен в список под номером index, если нет такого индекса - ошибка
     */
    public int get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("не верный индекс элемента");
            return -1;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Удаляет все элементы из списка
     */
    public void clear() {
        size = 0;
    }

    /**
     * Возвращает количество элементов списка
     *
     * @return размер списка
     */
    public int size() {
        return size;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая все элементы после удаляемого влево
     *
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
        size = size - 1;
    }

    /**
     * Удаляет первое вхождение элемента в список
     *
     * @param element элемент, который нужно удалить
     */
    public void remove(int element) {
        for (int i = 0; i < size - 1; i++) {
            if (elements[i] == element) {
                for (int j = i; j < size - 1; j++) {
                    elements[j] = elements[j + 1];

                }
                size = size - 1;
                return;
            }

        }
    }

    /**
     * Удаляет последнее вхождение элемента в список
     *
     * @param element элемент, который нужно удалить
     */
    public void removeLast(int element) {
        for (int i = size - 1; i > 0; i--) {
            if (elements[i] == element) {
                for (int j = i; j < size - 1; j++) {
                    elements[j] = elements[j + 1];

                }
                size = size - 1;
                return;
            }

        }

    }

    /**
     * Удалить все вхождения элемента в массив
     *
     * @param element элемент, который нужно удалить
     */
    public void removeAll(int element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                for (int j = i; j < size; j++) {
                    elements[j] = elements[j + 1];

                }
                size = size - 1;

            }

        }
    }

    /**
     * Вставляет элемент в заданный индекс (проверяет условие, index < size)
     * Элемент который стоял под индексом index сдвигается вправо со всеми следующими элементами
     *
     * @param index   индекс позицию, куда вставляем элемент
     * @param element элемент, которы вставляем
     */
    public void add(int index, int element) {
        addElementToIndex(index, element);


    }

    private void addElementToIndex(int index, int element) {
        if (index == size) {
            add(element);
        } else if (index < size) {
            size = size + 1;
            for (int i = size - 1; i > index; i--) {
                elements[i] = elements[i - 1];
            }
            elements[index] = element;
        } else {
            System.err.println("Не верно задан индекс");
        }
    }

}