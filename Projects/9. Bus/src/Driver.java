public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public String getName() {
        return name;
    }

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;

        } else {
            System.err.println("ОПЫТА у " + name + " НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void drive(Bus bus) {
        if (bus != this.bus) {
            System.err.println("Попытка несанкционированного запуска автобуса №" + bus.getNumber() + "! Нарушитель - " + name);
        } else if (!bus.isMoveStatus()) {
            bus.move();
        } else {
            System.err.println("Автобус уже едет!");
        }

    }

    public void stopBus (Bus bus) {
        if (bus != this.bus) {
            System.err.println(name + ", не может управлять не своим автобусом");
        } else if (bus.isMoveStatus()) {
            bus.stop();
        } else {
            System.err.println("Автобус уже стоит!");
        }
    }


}
