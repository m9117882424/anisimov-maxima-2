public class Passenger {

    private String name;

    private Bus bus;

    public Passenger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void goToBus(Bus bus) {
        if (this.bus != null) {
            System.err.println("Я, " + name + ", уже в автобусе!");
        } else {
            if (!bus.isFull()) {
                this.bus = bus;
                this.bus.incomePassenger(this);
            } else {
                System.err.println("Я, " + name + ", не попал в автобус");
            }

        }
    }

    public void leaveBus() {

    }
}
