public class Bus {


    private int number;
    private String model;
    private Driver driver;
    private Passenger[] passengers;
    private int count;
    private boolean moveStatus = false;

    public boolean isMoveStatus() {
        return moveStatus;
    }

    public int getNumber() {
        return number;
    }
    public String getModel() {
        return model;
    }

    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        this.passengers = new Passenger[placesCount];
    }

    public void incomePassenger(Passenger passenger) {
        if (moveStatus) {
            System.err.println(passenger.getName() + " не может войти в автобус, т.к. автобус движется!");
        } else {
            if (this.count < this.passengers.length) {
                this.passengers[count] = passenger;
                this.count++;
            } else {
                System.err.println("Автобус переполнен");
            }

        }
    }

    public void setDriver(Driver driver) {
        if (moveStatus) {
            System.err.println("Водитель " + this.driver.getName() + " не может смениться, т.к. автобус движется!");
        } else {
            this.driver = driver;
            driver.setBus(this);
        }
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }

    public void move() {
        moveStatus = true;
        System.out.println("едет автобус. " + number + " в нем едут:");
        for (int i = 0; i < passengers.length; i++) {
            if (passengers[i] == null) {
                System.out.println("место №" + (i + 1) + " свободно.");
            } else {
                System.out.println(passengers[i].getName());
            }
        }

    }

    public void stop() {
        System.err.println("Автобус остановлен");
        moveStatus = false;


    }

}


