public class Main {
    public static void main(String[] args) {

        Driver driver = new Driver("Семен", 10);
        Driver driver1 = new Driver("Рагим", 5);
        Bus bus = new Bus(99, "Нефаз", 5);
        Bus bus1 = new Bus(100, "МАЗ", 4);
        Passenger passenger1 = new Passenger("Максим");
        Passenger passenger2 = new Passenger("Юля");
        Passenger passenger3 = new Passenger("Саша");
        Passenger passenger4 = new Passenger("Оля");
        Passenger passenger5 = new Passenger("Юра");
        Passenger passenger6 = new Passenger("Володя");

        bus.setDriver(driver);

        passenger1.goToBus(bus);
        passenger2.goToBus(bus);
        passenger3.goToBus(bus);
        passenger4.goToBus(bus);
        passenger5.goToBus(bus);

        driver.drive(bus);
        passenger6.goToBus(bus);
        bus.setDriver(driver1);
        driver1.drive(bus);
        driver.drive(bus1);
        driver.stopBus(bus);
        driver1.stopBus(bus1);
        driver.stopBus(bus);


    }
}
