import java.util.ArrayList;
import java.util.Date;

public class Logger {
    //Date date = new Date();
    //singleton

    private final static Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {

    }

    public static Logger getInstance() {
        return instance;
    }

    private final ArrayList<String> tasks = new ArrayList<>();

    public void log(String message) {
        //что-то должен выводить
        tasks.add(message);
        Date date = new Date();
        System.out.println(date);
        System.out.println("выполнено дел: " + tasks.size());

        for (int i = 0; i < tasks.size(); i++) {

            System.out.println((i + 1) + ") " + tasks.get(i));
        }
        System.out.println("---------------------------------------");

    }
}

