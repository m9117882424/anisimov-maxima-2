package object;

import java.util.Scanner;

public class Main {

    public static void objectProcess(Object object){
        System.out.println(object.getClass());
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human human = new Human("Maks", 37, 1.76);

        objectProcess(scanner);
        objectProcess(human);

    }
}
