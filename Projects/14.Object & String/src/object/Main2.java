package object;

import java.util.Scanner;

public class Main2 {
    //true - когда все элементы равны
    //false - когда есть хотя бы один элемент, не равный остальным
    public static boolean  allEquals(Object[] objects) {
        for (int i = 1; i < objects.length; i++) {
            if (!objects[i - 1].equals(objects[i])) {
                return false;
            }
        }
        return true;
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human human1 = new Human("Maks", 37, 1.76);
        Human human2 = new Human("Maks", 37, 1.76);
        Human human3 = new Human("Maks", 37, 1.76);

        Object[] objects = {human1, human2, human3};
        System.out.println(allEquals(objects));

        System.out.println(human1 == human2); //false
        System.out.println(human1.equals(human2)); //false
        // по-умолчанию equals реализован через ==


    }
}
