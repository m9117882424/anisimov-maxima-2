package object;

public class Human {
    private String name;
    private int age;
    private double height;

    public Human(String name, int age, double height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    @Override
    public String toString() {
        return this.name + " " + this.age + " " + this.height;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }

        if (obj instanceof Human) {

            Human that = (Human) obj;
            return this.age == that.age &&
                    this.height == that.height &&
                    this.name.equals(that.name);
        }
        return false;
    }
//    public boolean equals(object.Human that) {
//        if (this == that) {
//            return true;
//        }
//        return this.age == that.age &&
//                this.height == that.height &&
//                this.name.equals(that.name);
//    }
}
