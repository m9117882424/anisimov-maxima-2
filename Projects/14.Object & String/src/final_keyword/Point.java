package final_keyword;

public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public final double getAngle(){
        return Math.atan2(x, y);
    }
}
