package final_keyword;

public class MainFinalPrimitiveVariable {
    public static void main(String[] args) {
        final int x = 10;
        System.out.println(x);

        // x = 15; final на разрешает изменить значение переменной

    }
}
