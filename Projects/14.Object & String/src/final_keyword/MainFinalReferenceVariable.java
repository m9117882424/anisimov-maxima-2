package final_keyword;

import object.Human;

public class MainFinalReferenceVariable {
    public static void main(String[] args) {
        final Human human = new Human("Maksim", 37, 1.76);
        human.setAge(28);
        human.setHeight(1.80);

        //тут final для того, чтобы нельзя было заменить ссылку на ссылочную переменную
    }
}
