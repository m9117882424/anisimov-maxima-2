package example5;

public class SequenceSelectionSortImpl extends Sequence {

    public SequenceSelectionSortImpl(int[] array) {
        super(array);
    }

    @Override
    public void sort() {

        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            // пробегаем все элементы массива
            for (int j = i; j < array.length; j++) {
                // как только нашли новый минимальный
                if (array[j] < min) {
                    // то запоминаем его значение и индекс
                    min = array[j];
                    indexOfMin = j;
                }
            }
            // как только нашли минимальный элемент, меняем его местами с самым первым
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;

        }
    }
}