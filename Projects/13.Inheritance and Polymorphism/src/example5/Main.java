package example5;

public class Main {
    public static void main(String[] args) {
        Sequence sequence = new SequenceBubbleSortImpl(new int[]{33, -10, 777, 600, 1});
        System.out.println(sequence.contains(33));
        System.out.println(sequence.contains(800));
        System.out.println(sequence.contains(-10));
        System.out.println(sequence.contains(777));

    }
}
