package example5;

public abstract class Sequence {
    protected int[] array;

    public Sequence(int[] array) {
        this.array = array;
        sort();
    }

    public abstract void sort();

    public boolean contains(int element) {
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
            } else if (element > array[middle]) {
                left = middle + 1;
            } else {
                return true;
            }
            middle = left + (right - left) / 2;
        }

        return false;

    }
}
