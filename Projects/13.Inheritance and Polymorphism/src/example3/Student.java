package example3;

public class Student extends Human{
    public Student(String firstName, String lastName){
        super(firstName,lastName);
    }

    @Override
    public void work() {
        System.out.println("Нет времени работать");
        study();
    }

    public void study(){
        System.out.println("Учится");
    }

}
