package example3;

public class Sportsman extends Human {
    public Sportsman(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void work() {
        System.out.println("Надо идти в зал");
        playGame();
    }

    public void playGame() {
        System.out.println("В игре");
    }


}
