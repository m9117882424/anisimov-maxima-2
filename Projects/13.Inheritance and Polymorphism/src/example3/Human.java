package example3;

public class Human {
    protected String firstName;
    protected String lastName;

    public Human(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void work() {
        System.out.println("Человек пошел на работу");
    }
}
