package example1;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Database database = new Database();

        User user = new User(1, "maks@mail.ru", "12345");
        User user1 = new User(2, "maks1@mail.ru", "12346");
        User user2 = new User(3, "maks2@mail.ru", "12347");
        User user3 = new User(4, "maks3@mail.ru", "12348");
        User user4 = new User(5, "maks4@mail.ru", "12340");

        Admin admin = new Admin(0, "admin@mail.ru", "qwer007", new String[]{"read", "delete", "update", "create"});
        System.out.println(Arrays.toString(admin.authenticate()));
        database.authenticateAdmin(admin);

        database.addUser(user);
        database.addUser(user1);
        //database.addUser(user2);
        database.addUser(user3);
        database.addUser(user4);

        database.authenticateUser(user);
        database.authenticateUser(user1);
        database.authenticateUser(user2);
    }
}
