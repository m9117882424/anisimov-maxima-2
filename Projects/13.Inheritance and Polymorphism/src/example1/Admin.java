package example1;

public class Admin extends User{

    private String[] authorities;

    public Admin(int id, String email, String password, String[] authorities) {
        super(id, email, password);
        this.authorities = authorities;
    }

    public String[] authenticate() {
        String result[] = new String[authorities.length + 2];
        result[0] = this.email;
        result[1] = this.password;

        for (int i = 2; i < result.length; i++) {
            result[i] = this.authorities[i - 2];
        }
        return result;
    }



}
