package example4;

public abstract class Figure {
    private double x;
    private double y;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void move(double newX, double newY ){
        this.x = newX;
        this.y = newY;
    }

    public abstract double getPerimeter();
}
