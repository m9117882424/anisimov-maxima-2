package example4;

public class Ellipse extends Figure{
    private double firstR;
    private double secondR;

    public Ellipse(double x, double y, double firstR, double secondR) {
        super(x, y);
        this.firstR = firstR;
        this.secondR = secondR;
    }

    @Override
    public double getPerimeter() {
        return 4 *(Math.PI * firstR * secondR + Math.pow((firstR-secondR), 2)/ (firstR + secondR));
    }
}
