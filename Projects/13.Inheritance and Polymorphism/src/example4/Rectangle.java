package example4;

public class Rectangle extends Figure{
    private int a;
    private int b;

    public Rectangle(double x, double y, int a, int b) {
        super(x,y);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return 2 * a + 2 * b;
    }
}
