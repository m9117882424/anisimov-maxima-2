package example4;

public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(10, 15, 5, 7);
        Circle circle = new Circle(5, 6, 10);
        Rectangle rectangle = new Rectangle(1, 1, 56, 10);
        Square square = new Square(10, 15, 6);

        Figure[] figures = {ellipse, circle, rectangle, square};

        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i].getPerimeter());

        }
    }
}
