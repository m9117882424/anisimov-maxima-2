package example6;

public class Box implements Shape, SpaceObject{
    private double x;
    private double y;
    private double z;

    @Override
    public void move(double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    public Box(double x, double y, double z, double length, double height, double width) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.length = length;
        this.height = height;
        this.width = width;
    }

    private double length;
    private double height;
    private double width;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public void scale(double value){
        this.length *= value;
        this.height *= value;
        this.width *= value;
    }

}
