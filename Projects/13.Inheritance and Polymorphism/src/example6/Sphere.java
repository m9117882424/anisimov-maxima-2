package example6;

public class Sphere implements Shape, SpaceObject {

    private double x;
    private double y;
    private double z;

    private double radius;

    public Sphere(double x, double y, double z, double radius) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.radius = radius;
    }

    @Override
    public void scale(double value){
        this.radius *= value;
    }

    @Override
    public void move(double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }
}
