package example6;

public interface Shape {
    void scale(double value);
}
