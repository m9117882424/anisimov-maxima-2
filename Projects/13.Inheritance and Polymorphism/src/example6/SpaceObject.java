package example6;

public interface SpaceObject {
    void move(double newX, double newY, double newZ);
}
