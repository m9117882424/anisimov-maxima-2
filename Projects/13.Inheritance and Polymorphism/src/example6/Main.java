package example6;

public class Main {
    public static void main(String[] args) {
        Box box = new Box(5, 5, 5, 10, 20, 30);
        Sphere sphere = new Sphere(0,0,0,10);
        Point point = new Point(1,1,1);

        Shape[] shapes = {box, sphere};

        box.scale(2);
        sphere.scale(0.5);

        System.out.println(box.getHeight() + " " + box.getLength() + " " + box.getWidth());

    }
}
