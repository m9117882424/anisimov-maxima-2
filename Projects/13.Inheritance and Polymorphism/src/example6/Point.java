package example6;

public class Point implements SpaceObject{
    private double x;
    private double y;
    private double z;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void move(double newX, double newY, double newZ){
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }
}
