drop table food_products;

create table food_products(
                              id_product serial primary key,
                              name_product char(40) not null,
                              price int default 0 not null check ( price > -1 ),
                              expiration_date date not null check ( expiration_date > delivery_date ),
                              delivery_date date not null,
                              supplier char(40) not null
);

insert into food_products (name_product, price, expiration_date, delivery_date, supplier) values ('молоко', 1500, '2022-02-15', '2022-02-03', 'простаквашино');
insert into food_products (name_product, price, expiration_date, delivery_date, supplier) values ('батон', 60, '2022-02-15', '2022-01-10','веселый пекарь');
insert into food_products (name_product, price, expiration_date, delivery_date, supplier) values ('Торот Наполеон', 600, '2022-02-15', '2022-01-28','Карат');
insert into food_products (name_product, price, expiration_date, delivery_date, supplier) values ('черная икра', 8700, '2022-02-15', '2022-02-10','веселый роджер');

select name_product, price from food_products where price > 100;
select name_product, delivery_date from food_products where delivery_date::date < '2022-02-02';
select supplier from food_products;