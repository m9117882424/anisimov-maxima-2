public class Nullable<T> {
    private T value;

    private Nullable(T value) {
        this.value = value;
    }

    public static <T> Nullable<T> empty() {
        return new Nullable<>(null);
    }
}