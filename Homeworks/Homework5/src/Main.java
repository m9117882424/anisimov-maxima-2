public class Main {

    public static void main(String[] args) {

        int[] ageArr = new int[]{10, 10, 10, 30, 15, 45, 15, 60, 10, 15}; //test

        int[] countAges = new int[ageArr.length];
        int popularAge = ageArr[0];

        // массив количества повторений
        for (int i = 0; i < ageArr.length; i++) {
            for (int j = 0; j < ageArr.length; j++) {
                if (ageArr[i] == ageArr[j]) {
                    countAges[i]++;
                }
            }
        }
        //тест
        //System.out.println(Arrays.toString(countAges));

        //вывод часто повторяющегося возраста
        int topAge = countAges[0];
        for (int i = 0; i < countAges.length; i++) {
            if (countAges[i] > topAge) {
                topAge = countAges[i];
                popularAge = ageArr[i];
            }
            if ((topAge == countAges[i]) && (ageArr[i] < popularAge)) {
                popularAge = ageArr[i];
            }
        }
        System.out.println(popularAge);
    }
}
