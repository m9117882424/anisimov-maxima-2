import java.util.Arrays;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        int[] array = {-4, 1, 10, 11, 13, 20, 35, 46, 56, 67};
        Scanner scanner = new Scanner(System.in);
        int element = scanner.nextInt();
        System.out.println(search(array, element));
        selectionSort(array);

    }

    public static boolean search(int[] array, int element) {
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;
        boolean hasElement = false;

        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
            } else if (element > array[middle]) {
                left = middle + 1;
            } else {
                hasElement = true;
                break;
            }
            middle = left + (right - left) / 2;
        }
        return hasElement;
    }

    public static void selectionSort(int[] array) {
        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
        System.out.println(Arrays.toString(array));
    }
}
