import java.util.Scanner;

// HW3 count of local MIN

public class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int count = 0;

        while (a != -1 && b != -1 && c != -1) {
            if (b < a && b < c) {
                count++;
            }
            a = b;
            b = c;
            c = scanner.nextInt();
        }
        System.out.println(count);
    }
}

