import java.util.Scanner;

public class Program1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int digitsSum = 0;
        int answer = 0;
        int temp = 0;

        while (number != -1) {
            answer = number;
            while (number != 0) {
                int lastDigit = number % 10;
                number = number / 10;
                digitsSum = digitsSum + lastDigit;
            }
            if (temp <= digitsSum) temp = digitsSum;

            number = scanner.nextInt();
        }

        System.out.println(answer);

    }
}
