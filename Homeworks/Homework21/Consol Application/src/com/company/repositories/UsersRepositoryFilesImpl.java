package com.company.repositories;

import com.company.models.User;
import com.company.util.IdGenerator;

import java.io.*;
import java.util.Optional;

public class UsersRepositoryFilesImpl implements UsersRepository {
    private final String fileName;
    private final IdGenerator generator;
    public UsersRepositoryFilesImpl(String fileName, IdGenerator generator){
        this.fileName = fileName;
        this.generator = generator;
    }
    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))){
            user.setId(generator.generate());
            writer.write(user.getId() + "|" + user.getEmail() + "|" + user.getPassword());
            writer.newLine();
        } catch (IOException e) {
            throw  new IllegalArgumentException(e);
        }

    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String userLine = reader.readLine();

            while (userLine != null) {
                String[] parsedUserLine = userLine.split("\\|");
                if (parsedUserLine[1].equals(email)){
                    String idFromFile = parsedUserLine[0];
                    String emailFromFile = parsedUserLine[1];
                    String passwordFromFile= parsedUserLine[2];

                    User user = new User(Long.parseLong(idFromFile),emailFromFile, passwordFromFile);
                    return Optional.of(user);
                }
                userLine = reader.readLine();
            }
            return Optional.empty();
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }
}
