package com.company.services;

import com.company.services.exceptions.AuthenticationException;
import com.company.validators.exceptions.UnuniqueEmailException;

public interface UsersService {
    /**
     * Регистрирует пользователя в системе
     * @param email Email пользователя
     * @param password пароль пользователя
     */
    void signUp(String email, String password);

    /**
     * Проверяет, есть ли пользователь в базе данных с такими учетными данными
     * @param email Email пользователя
     * @param password пароль пользователя
     * @throws AuthenticationException в случае если  email/пароль не верные
     * @throws UnuniqueEmailException в случве повторного email
     */

    void signIn(String email, String password) throws AuthenticationException;



}
