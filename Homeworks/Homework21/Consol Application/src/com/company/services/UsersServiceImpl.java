package com.company.services;

import com.company.models.User;
import com.company.repositories.UsersRepository;
import com.company.services.exceptions.AuthenticationException;
import com.company.validators.EmailValidator;
import com.company.validators.PasswordValidator;
import com.company.validators.exceptions.UnuniqueEmailException;

import java.util.Optional;

public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    public UsersServiceImpl(UsersRepository usersRepository,
                            EmailValidator emailValidator,
                            PasswordValidator passwordValidator) {
        this.usersRepository = usersRepository;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }


    public void signUp(String email, String password) {

        emailValidator.validate(email);
        passwordValidator.validate(password);

        User user = new User(email, password);
        Optional<User> userOptional = usersRepository.findByEmail(email);
        if (userOptional.isPresent()) {
            throw new UnuniqueEmailException();
        }

        usersRepository.save(user);

    }

    @Override
    public void signIn(String email, String password) throws AuthenticationException {
        //находим пользователя по email
        Optional<User> userOptional = usersRepository.findByEmail(email);

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationException();
            }
            return;
        }
        throw new AuthenticationException();
    }


}


