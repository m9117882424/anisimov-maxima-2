package com.company.validators;

import com.company.validators.exceptions.EmailValidationException;

public interface EmailValidator {
    void validate(String email) throws EmailValidationException;
}
