package com.company.validators;

import com.company.validators.exceptions.PasswordValidationException;

public interface PasswordValidator {
    void validate (String password) throws PasswordValidationException;
}
