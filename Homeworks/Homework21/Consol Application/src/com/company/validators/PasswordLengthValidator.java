package com.company.validators;

import com.company.validators.exceptions.PasswordValidationException;

public class PasswordLengthValidator implements PasswordValidator {
    @Override
    public void validate(String password) throws PasswordValidationException {
        if(password.length() < 5) {
            throw new PasswordValidationException();
        }
    }
}
