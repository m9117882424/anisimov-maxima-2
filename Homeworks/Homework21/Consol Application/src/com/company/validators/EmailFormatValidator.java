package com.company.validators;

import com.company.validators.exceptions.EmailValidationException;

public class EmailFormatValidator implements EmailValidator {

    @Override
    public void validate(String email) throws EmailValidationException {
        if (!email.contains("@")) {
            throw new EmailValidationException();
        }
    }
}
