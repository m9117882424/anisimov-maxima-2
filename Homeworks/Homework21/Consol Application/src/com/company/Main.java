package com.company;

import com.company.repositories.UsersRepository;
import com.company.repositories.UsersRepositoryFilesImpl;
import com.company.services.UsersServiceImpl;
import com.company.util.IdGenerator;
import com.company.util.IdGeneratorFilesImpl;
import com.company.validators.*;

public class Main {

    public static void main(String[] args) {
        IdGenerator idGenerator = new IdGeneratorFilesImpl("users_id_sequence.txt");
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt", idGenerator);
        EmailValidator emailValidator = new EmailFormatValidator();
        PasswordValidator passwordValidator = new PasswordLengthValidator();
        UsersServiceImpl usersServiceImpl = new UsersServiceImpl(usersRepository, emailValidator, passwordValidator);
        usersServiceImpl.signUp("m9117882425@gmail.com", "987654");
        //usersServiceImpl.signIn("m9117882424@gmail.com", "987654");
    }
}
