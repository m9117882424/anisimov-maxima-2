package com.company.util;

public interface IdGenerator {
    Long generate();
}
