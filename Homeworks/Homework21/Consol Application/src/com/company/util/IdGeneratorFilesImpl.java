package com.company.util;

import java.io.*;

public class IdGeneratorFilesImpl implements IdGenerator {

    private final String fileName;

    public IdGeneratorFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Long generate() {
        Long lastGeneratedId = getLastGeneratedId();
        Long newId = lastGeneratedId + 1;
        writeLastGeneratedId(newId);
        return newId;
    }

    private void writeLastGeneratedId(Long id) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(id.toString());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    private Long getLastGeneratedId() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String lastGeneratedId = reader.readLine();
            return Long.parseLong(lastGeneratedId);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
